<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookMail extends Model
{
    //
    protected $fillable = ['name','email'];
}
