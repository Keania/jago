<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEbookMail extends Mailable
{
    use Queueable, SerializesModels;

    public $request;
    public $book;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request,$book)
    {
        //
        $this->request = $request;
        $this->book = $book;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $file_path = public_path().'/storage/books/'.$this->book->url;
        
        return $this->from('keaniaeric@gmail.com')
                ->subject('Ebook From Emmanuel Jago')
                ->view('emails.sendBook')
                ->with([
                    'name'=>$this->request->name
                ])
                ->attach($file_path);
    }
}
