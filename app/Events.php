<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\EventInterest;

class Events extends Model
{
    //
    protected $table = 'events';


    public function scopeSearch($query,$s){
        return $query->where('title','like','%'.$s.'%')->orWhere('body','like','%'.$s.'%');
    }

    public function scopeHasParticipants($query){
        return $query->where('event_interest','>',0);
    }

    public function event_interest(){
        return $this->hasMany('App\EventInterest','event_id');
    }

    
}
