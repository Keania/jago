<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Events;
use Illuminate\Support\Facades\Mail;
use Exception;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        $all_posts = Post::all();
        $all_events = Events::all();

        $posts =  $all_posts->count() >= 6 ? $all_posts->random(6) : $all_posts;
        $events = $all_events->count() >= 3 ? $all_events->random(3) : $all_events;
       
        return view('front.home',['posts'=>$posts,'events'=>$events]);
    }
    public function blog(){
        $all_posts = Post::all();

        $posts = Post::latest()->paginate(10);
        $sidePost5 = $all_posts->count() >= 5 ? Post::all()->random(5) : $all_posts;
        $sidePost4 = $all_posts->count() >= 4 ? Post::all()->random(4) : $all_posts;
        return view('front.blog.index',['posts'=>$posts,'sidePost5'=>$sidePost5,'sidePost4'=>$sidePost4]);
    }

    public function event(){
        $events = Events::latest()->paginate(10);

        return view('front.event.index',['events'=>$events]);
    }

    public function single_event(Request $request){
        $event = Events::find($request->id);
        return view('front.event.single',['event'=>$event]);

    }
    public function contact(){
        return view('front.contact');
    }
    public function about(){
        return view('front.about');
    }
    public function single(Request $request){
        $post = Post::find($request->id);
        return view('front.blog.single',['post'=>$post]);
    }
    public function search(Request $request){
        $s = $request->search;
        $posts = Post::latest()->search($s)->paginate(6) ? Post::latest()->search($s)->paginate(6) : Post::latest()->search($s)->all();
        return view('front.blog.search',['posts'=>$posts]);
        

    }

    public function sendContactMsg(Request $request){

        try{

            Mail::send('emails.contact',['name'=>$request->name,'content'=>$request->message],function($message) use ($request) {
                $message->to('keaniaeric@gmail.com');
                $message->replyTo($request->email);
            });
        }catch(Exception $e){

            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }

        return redirect()->back()->with(['success'=>'Email was sent we will contact you very soon. Thank you']);
       
    }
    
}
