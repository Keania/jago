<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events;
use Illuminate\Support\Facades\Storage;
use Exception;
use Illuminate\Database\QueryException;
use App\EventInterest;


class EventController extends Controller
{
    //
    public function __construct(){
        $this->storage = Storage::disk('events');
       
        
    }
    
    public function add(Request $request){
        
       
        try{

            $event = new Events();
            $event->title = $request->title;
            $event->body = $request->body;
            $event->venue = $request->venue;
            $event->time  = $request->time;
            $event->date = $request->date;
            $filename = null;
            if($request->hasFile('image') && $request->file('image')->isValid()){
                $filename =  $this->storage->putFile('',$request->file('image'));
            }
            if($filename){
                $event->image = $filename;
            }
            $event->save();

        }catch(QueryException $e){

            return redirect()->back()->with(['error'=>$e->errorInfo[2]]);

        }catch(Exception $e){

            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }
       
        return redirect()->back()->with(['success'=>'Event was added successfully']);
    }

    public function delete(Request $request){

        try {

            $event = Events::find($request->id);
            $this->storage->delete($event->image);
            $event->delete();

        }catch(QueryException $e){

            return redirect()->back()->with(['error'=>$e->errorInfo[2]]);

        }catch(Exception $e){
            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }
       
        return redirect()->back()->with(['success'=>'Event deleted sucessfully']);
    }
    public function update(Request $request){

       try{
            $event = Events::find($request->id);
            !isset($request->title) ? :$event->title = $request->title ;
            !isset($request->body) ? :$event->body = $request->body ;
            !isset($request->time) ? :$event->time = $request->time ;
            !isset($request->date) ? :$event->date = $request->date ;
            !isset($request->venue) ? :$event->venue = $request->venue ;
            $filename = null;
            if($request->hasFile('image') && $request->file('image')->isValid()){
                $filename =  $this->storage->putFile('events',$request->file('image'));
            }
        if($filename){
            $event->image = $filename ;
        }
        
            $event->save();
       }catch(QueryException $e){

        return redirect()->back()->with(['error'=>$e->errorInfo[2]]);
        
       }catch(Exception $e){
           return redirect()->back()->with(['error'=>$e->getMessage()]);
       }
        return redirect()->back()->with(['success'=>'Event Details was successfully updated !']);
    }


    public function interestInEvent(Request $request){

       try{ 

            $event_interest = new EventInterest();
            $event_interest->event_id = $request->event_id;
            $event_interest->full_name  = $request->full_name;
            $event_interest->phone = $request->phone;
            $event_interest->email = $request->email;
            $event_interest->save();

        }catch(QueryException $e){

            return redirect()->back()->with(['error'=>$e->errorInfo[2]]);

        }catch(Exception $e){

            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }

        return redirect()->back()->with(['success'=>'Thanks for successfully registering to come for our event']);
    }
}
