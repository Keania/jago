<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Book;
use Illuminate\Support\Facades\DB;
use App\Events;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;


class AdminController extends Controller
{
    //
    public function dashboard(){
        return view('admin.dashboard');
    }
    public function blog(){
        $posts = Post::latest()->paginate(6);
        return view('admin.blog',['posts'=>$posts]);
    }
    public function book(){
       
       $books = DB::table('books')->get();
       $activebook = Book::where('active','=',true)->first();
      
        return view('admin.book',['books'=>$books,'activebook'=>$activebook]);
    }

    public function event(){

        $events = Events::latest()->paginate(6);
        return view('admin.events',['events'=>$events]);
    }

    public function misc(){
        return view('admin.misc');
    }

    public function eventParticipant(Request $request,$id){
       
        $event = Events::find($id);
        return $event->event_interest;
    }

    public function downloadEmails(){
       $book_mails = DB::table('book_mails')->select('email')->get();
        $event_interest  = DB::table('event_interests')->select('email')->get();
        $comments = DB::table('comments')->select('email')->get();
        $email_collection  = $book_mails->concat($event_interest)->concat($comments);

        if(Storage::disk('public')->exists('email.txt')){

            Storage::disk('public')->delete('email.txt');
        }

        foreach($email_collection as $email){
            
            Storage::disk('public')->append('email.txt',$email->email);
        }
       
        $file_url = Storage::url('email.txt');
         $file =  public_path().$file_url;
      
     
         $headers = array(
             'Content-Type: '.mime_content_type($file),
         );



         return response()->download($file,'user_emails.txt',$headers);
       
    }

}
