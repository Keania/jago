<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BookMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\QueryException;
use App\Book;
use App\Mail\SendEbookMail;

class EmailController extends Controller
{
    //
    public function download(){

    }
    public function getbook(Request $request){

        try{

            $booker = new BookMail();
            $booker->name = $request->name;
            $booker->email = $request->email;
            $booker->save();
    
            $this->sendEmail($request);
        }catch(QueryException $e){

            return redirect()->back()->with(['error'=>$e->errorInfo[2]]);
        }catch(Exception $e){
            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }

        

        return redirect()->back()->with(['success'=>'Your email was recieved we will send you an email shortly']);
       
       
        
    }

    public function sendEmail(Request $request){

        $book = Book::where('active',1)->first();
        if(! isset($book)){
            return ;
        }
        Mail::to($request->email)->send(new SendEbookMail($request,$book));

    }
}
