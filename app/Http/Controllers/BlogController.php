<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Post;
use App\Comment;

class BlogController extends Controller
{
    //
    public function __construct(){
        $this->storage = Storage::disk('posts');
       
        
    }

    public function add(Request $request){

        try {

            $post = new Post();
            $post->title = $request->title;
            $post->body = $request->body;
            $filename = 'default.jpg';
            if($request->hasFile('image') && $request->file('image')->isValid()){
                $filename =  $this->storage->putFile('',$request->file('image'));
            }
            if($filename){
                $post->image = $filename;
            }
            $post->save();
            
        }catch(QueryException $e){

            return redirect()->back()->with(['error'=>$e->errInfo[2]]);
        }catch(Exception $e){

            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }
        return redirect()->back()->with(['success'=>'Post was successfully added']);
    }

    public function delete(Request $request){
       try{

            $post = Post::find($request->id);
            $this->storage->delete($post->image);
            $post->delete();
       }catch(QueryException $e){

            return redirect()->back()->with(['error'=>$e->errInfo[2]]);

       }catch(Exception $e){

            return redirect()->back()->with(['error'=>$e->getMessage()]);
       }
        return redirect()->back()->with(['success'=>'Post was successfully deleted']);
    }
    public function update(Request $request){
       try{

            $post = Post::find($request->id);
            $post->title = $request->title;
            $post->body = $request->body;
            $filename = null;
            if($request->hasFile('image') && $request->file('image')->isValid()){
                $filename =  $this->storage->putFile('posts',$request->file('image'));
            }
        if($filename){
            $post->image = $filename ;
        }
        
            $post->save();
       }catch(QueryException $e){

           return redirect()->back()->with(['error'=>$e->errInfo[2]]);
       }catch(Exception $e){

        return redirect()->back()->with(['error'=>$e->getMessage()]);
       }
        return redirect()->back()->with(['success'=>'Post was succcessfully updated']);
    }
    public function comment(Request $request){

        try{

            $comment = new Comment();
            $comment->email = $request->email;
            $comment->comment = $request->comment;
            $comment->post_id = $request->post_id;
            $comment->save();
        }catch(QueryException $e){

            return redirect()->back()->with(['error'=>$e->errInfo[2]]);
        }catch(Exception $e){
            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }
        return redirect()->back()->with(['success'=>'Comment was added successfully']);
    }
}