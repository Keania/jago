<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use Illuminate\Support\Facades\Storage;
use Exception;
use Illuminate\Database\QueryException;

class BookController extends Controller
{
    //

    public function __construct(){
        $this->storage = Storage::disk('books');
      
    }

    public function add(Request $request){
        try{

            $book = new Book();
            $book->title = $request->title;
            $book->active = false;
            $bookimage = null;
            $bookfile = null;
            if($request->hasFile('image') && $request->file('image')->isValid()){
                $bookimage = $this->storage->putFile('images',$request->file('image'));
            
            }
            if($request->hasFile('book_file') && $request->file('book_file')->isValid()){
                $bookfile = $this->storage->putFile('files',$request->file('book_file'));
                
            }
            if($bookimage){
                $book->image = $bookimage;
            }
            if($bookfile){
                $book->url = $bookfile;
            }
            $book->save();

        }catch(QueryException $e){
            return redirect()->back()->with(['error'=>$e->errorInfo[2]]);
        }catch(Exception $e){
            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }
        
        return redirect()->back()->with(['success'=>$book->title.' was added successfully']);

        
    }
    public function delete(Request $request){
        $book = Book::find($request->id);
       
        if($book->image){
            $this->storage->delete($book->image);
        }
        try{
            
            $this->storage->delete($book->url);
            $book->delete();
    
        }catch(Exception $e){
            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }
       
        return redirect()->back()->with(['success'=>'Book was successfully deleted']);
    }
    public function activate(Request $request){
       
       try{
            $old_book = Book::where('active','=',true)->update(['active'=>false]);
            $new_book = Book::find($request->id)->update(['active'=>true]);
       }catch(Exception $e){
            return redirect()->back()->with(['error'=>$e->getMessage()]);
       }
       
        return redirect()->back()->with(['success'=>'New Book made Active']);

    }

    public function deactivate(){
        try{
            Book::where('active','=',true)->update(['active'=>false]);
        }catch(Exception $e){
            return redirect()->back()->with(['error'=>$e->getMessage()]);
        }
       
        return redirect()->back()->with(['success'=>'Book was successfully deactivated']);
    }
}
