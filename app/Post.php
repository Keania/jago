<?php

namespace App;
use App\Comment;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //

    public function comments(){
        return $this->hasMany('App\Comment');
    }

    public function scopeSearch($query,$s){
        return $query->where('title','like','%'.$s.'%')->orWhere('body','like','%'.$s.'%');
    }
}
