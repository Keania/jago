<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //

    public function blur_email(){
       
        return '@'.substr($this->email,0,7).str_random(7);
    }
}
