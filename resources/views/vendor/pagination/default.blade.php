@if ($paginator->hasPages())
    <ul class="">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="disabled"><button style="background-color:grey" class="pgn__prev pgn__num" disabled href="#"></button></li>
        @else
            <li><a class="pgn__num pgn__prev" href="{{ $paginator->previousPageUrl() }}" rel="prev"></a></li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="disabled"><span>{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li ><span class="pgn__num current">{{ $page }}</span></li>
                    @else
                        <li><a class="pgn__num" href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li><a class="pgn__num pgn__next" href="{{ $paginator->nextPageUrl() }}" rel="next"></a></li>
        @else
            <li class="disabled"><button style="background-color:grey" href="#" class="pgn__num pgn__next" disabled></button></li>
        @endif
    </ul>
@endif
