<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contact From Site</title>
</head>
<body>
    <section>
        <header>You been Contacted by {{$name}}</header>
        <p>{{$content}}</p>
        
    </section>
</body>
</html>