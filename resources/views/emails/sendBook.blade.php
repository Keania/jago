<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <section>
        <h4>Dear {{$name}}</h4>
        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Non corporis aspernatur nobis eligendi unde eum perferendis. Eligendi harum eius ratione voluptatem provident nemo, sint pariatur doloremque officiis velit voluptatum minima.</p>
        <p>Thanks for getting my book</p>
        <p>The book is sent as an attachment to this mail</p>

    </section>
    <footer>
        Thanks You,
        Emmanuel Jago
    </footer>
</body>
</html>