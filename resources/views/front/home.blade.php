@extends('front.app')
@section('extra-home__classes')
s-pageheader--home
@endsection
@section('page-header-content')

<div class="pageheader-content row">
            @if($events->count() > 0)
            <div class="col-full">

                <div class="featured">

                    <div class="featured__column featured__column--big">
                        <div class="entry" style="background-image:url('{{asset('storage/events/'.$events[0]->image)}}'); background-size:100% 100%">
                            
                            <div class="entry__content">
                                <!-- <span class="entry__category"><a href="#0">Music</a></span> -->

                                <h1><a href="{{route('single.event',['id'=>$events[0]->id])}}" title="">{{$events[0]->title}}</a></h1>

                                <div class="entry__info">
                                    <a href="#0" class="entry__profile-pic">
                                        <img class="avatar" src="images/avatars/user-03.jpg" alt="">
                                    </a>

                                    <ul class="entry__meta">
                                        <li><a href="#0"><i class="fa fa-map-marker"></i>{{$events[0]->venue}}</a></li>
                                        <li><i class="fa fa-calendar"></i>{{date('D M d, Y',strtotime($events[0]->date))}}</li>
                                        <li><i class="fa fa-clock-o"></i>{{date('h : i a',strtotime($events[0]->time))}}</li>
                                    </ul>
                                </div>
                            </div> <!-- end entry__content -->
                            
                        </div> <!-- end entry -->
                    </div> <!-- end featured__big -->

                    <div class="featured__column featured__column--small">

                        @if(isset($events[1]))
                        <div class="entry" style="background-image:url('{{asset('storage/events/'.$events[1]->image)}}');">
                            
                            <div class="entry__content">
                                <!-- <span class="entry__category"><a href="#0">Management</a></span> -->

                                <h1><a href="{{route('single.event',['id'=>$events[1]->id])}}" title="">{{$events[1]->title}}</a></h1>

                                <div class="entry__info">
                                    <a href="#0" class="entry__profile-pic">
                                        <img class="avatar" src="images/avatars/user-03.jpg" alt="">
                                    </a>

                                    <ul class="entry__meta">
                                        <li><a href="#0"><i class="fa fa-map-marker"></i>{{$events[1]->venue}}</a></li>
                                        <li><i class="fa fa-calendar"></i>{{date('D M d, Y',strtotime($events[1]->date))}}</li>
                                        <li><i class="fa fa-clock-o"></i>{{date('h : i a',strtotime($events[1]->time))}}</li>
                                    </ul>
                                </div>
                            </div> <!-- end entry__content -->
                          
                        </div> <!-- end entry -->
                        @endif

                        @if(isset($events[2]))
                        <div class="entry" style="background-image:url('{{asset('storage/events/'.$events[2]->image)}}');">

                            <div class="entry__content">
                                <!-- <span class="entry__category"><a href="#0">LifeStyle</a></span> -->

                                <h1><a href="{{route('single.event',['id'=>$events[2]->id])}}" title="">{{$events[2]->title}}</a></h1>

                                <div class="entry__info">
                                    <a href="#0" class="entry__profile-pic">
                                        <img class="avatar" src="images/avatars/user-03.jpg" alt="">
                                    </a>

                                    <ul class="entry__meta">
                                        <li><a href="#0"><i class="fa fa-map-marker"></i>{{$events[2]->venue}}</a></li>
                                        <li><i class="fa fa-calendar"></i>{{date('D M d, Y',strtotime($events[2]->date))}}</li>
                                        <li><i class="fa fa-clock-o"></i>{{date('h : i a',strtotime($events[2]->time))}}</li>
                                    </ul>
                                </div>
                            </div> <!-- end entry__content -->

                        </div> <!-- end entry -->
                        @endif

                    </div> 
                    <!-- end featured__small -->
                </div> <!-- end featured -->

            </div> <!-- end col-full -->
            @endif
        </div>

@endsection
@section('page-content')
<section class="s-content">
        
        <div class="row masonry-wrap">
            <div class="masonry">
           

                <div class="grid-sizer"></div>
               

                @foreach($posts as $post)

                    <article class="masonry__brick entry format-standard" data-aos="fade-up">
                            
                        <div class="entry__thumb" style="height:120px;">
                            <a href="{{route('blog.single',['id'=>$post->id])}}" class="entry__thumb-link">
                                <img src="{{asset('storage/posts/'.$post->image)}}" 
                                        alt="">
                            </a>
                        </div>
        
                        <div class="entry__text">
                            <div class="entry__header">
                                
                                <div class="entry__date">
                                    <a href="{{route('blog.single',['id'=>$post->id])}}">{{date('F d. Y',strtotime($post->created_at))}}</a>
                                </div>
                                <h1 class="entry__title"><a href="{{route('blog.single',['id'=>$post->id])}}">{{$post->title}}</a></h1>
                                
                            </div>
                            <div class="entry__excerpt">
                                <p>
                                    {{substr($post->body,0,200)}}
                                </p>
                            </div>
                            <div class="entry__meta">
                                <span class="entry__meta-links">
                                    <a href="{{route('blog.single',['id'=>$post->id])}}">Read More ..</a> 
                                   
                                </span>
                            </div>
                        </div>
        
                    </article> <!-- end article -->

                @endforeach

               

               

            </div> <!-- end masonry -->
        </div> <!-- end masonry-wrap -->

       

    </section> <!-- s-content -->
@endsection


		