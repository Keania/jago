@extends('front.app')
@section('page-content')
<section class="s-content s-content--narrow s-content--no-padding-bottom">

	<article class="row format-gallery">

		<div class="s-content__header col-full">
			
			<h1 class="s-content__header-title">
				{{$event->title}}
			</h1>
			<ul class="s-content__header-meta">
				<li><i class="fa fa-bell-o"></i>{{count($event->event_interest)}} Registered</li>
				<li class="date"><i class="fa fa-map-marker"></i> {{$event->venue}}</li>
				<li class="date"><i class="fa fa-calendar"></i> {{date('D d,M Y',strtotime($event->date))}}</li>
				<li class="date"><i class="fa fa-clock-o"></i> {{date('H:i:m a',strtotime($event->time))}}</li>
				
			</ul>
		</div> <!-- end s-content__header -->

		<div class="s-content__media col-full">
			<div class="s-content__slider slider">
				<div class="slider__slides">
					<div class="slider__slide">
						<img src="{{asset('storage/events/'.$event->image)}}" 
							
							sizes="(max-width: 2000px) 100vw, 2000px" alt="" >
					</div>
					
				</div>
			</div>
		</div> <!-- end s-content__media -->

		<div class="col-full s-content__main">

			
			
			<p>{{$event->body}}
			</p>

			
			@php
				$prevId = (int)$event->id - 1;
				$nextId = (int)$event->id + 1;

				if($prevId > 0 ){
					$prevEvents = App\Events::find($prevId);
				}

				try{
					$nextEvents = App\Events::findOrFail($nextId);

				}catch(ModelNotFoundException $e){
					$nextEvents = null;
				}catch(Exception $e){
					$nextEvents = null;
				}

			@endphp
			<div class="">
				<caption><h5>Fill Your Details Below to indicate interest in Event</h5></caption>
				<br>

				<form method="POST" action="{{route('interest.event')}}">
					{{csrf_field()}}
					<input type="hidden" value="{{$event->id}}" name="event_id">
					<div>
						<label for="full_name">Full name</label>
						<input type="text" name="full_name" class="full-width" required>
					</div>
					<div>
						<label for="email"> Your Email </label>
						<input type="text" name="email" class="full-width">
					</div>
					<div>
						<label for="phone"> Your Phone </label>
						<input type="text" name="phone" class="full-width" required>
					</div>
					<div>
						<input type="submit" name="submit" class="btn">
					</div>
				</form>
				
			</div>
			
			<div class="s-content__pagenav">
				<div class="s-content__nav">
					@if(isset($prevEvents))
						<div class="s-content__prev">
							<a href="{{route('single.event',['id'=>$prevId])}}" rel="prev">
								<span>Previous Events</span>
								{{$prevEvents->title}}
							</a>
						</div>
					@endif
					@if(isset($nextEvents))
						<div class="s-content__next">
							<a href="{{route('single.event',['id'=>$nextId])}}" rel="next">
								<span>Next Events</span>
								{{$nextEvents->title}}
							</a>
						</div>
					@endif
				</div>
			</div> <!-- end s-content__pagenav -->
			<br>

		</div> <!-- end s-content__main -->

	</article>




</section> <!-- s-content -->
@endsection