@extends('front.app')

@section('page-content')
<style>
	.event__holder{
		box-shadow: 0 2px 3px rgba(0, 0, 0, 0.05)
	}

</style>
<section class="s-content">
        
        <div class="row masonry-wrap">
            <div class="masonry">
           

                <div class="grid-sizer"></div>
               

                @foreach($events as $event)

                    <article class="masonry__brick entry format-standard" data-aos="fade-up">
                            
					<div class="entry__thumb" style="height:120px;">
                            <a href="{{route('single.event',['id'=>$event->id])}}" class="entry__thumb-link">
                                <img src="{{asset('storage/events/'.$event->image)}}" 
                                        alt="">
                            </a>
                        </div>
        
                        <div class="entry__text">
                            <div class="entry__header">
                                
                                <div class="entry__date">
                                    <a href="{{route('single.event',['id'=>$event->id])}}">{{date('F d. Y',strtotime($event->created_at))}}</a>
                                </div>
                                <h1 class="entry__title"><a href="{{route('single.event',['id'=>$event->id])}}">{{$event->title}}</a></h1>
                                
                            </div>
                            <div class="entry__excerpt">
                                <p>
                                   <ul>
									   <li><i class="fa fa-map-marker"></i>{{$event->venue}}</li>
									   <li><i class="fa fa-calendar"></i>{{date('D M Y',strtotime($event->date))}}</li>
									   <li><i class="fa fa-clock-o"></i>{{date('H:i:m a',strtotime($event->time))}}</li>
								   </ul>
                                </p>
                            </div>
                            <div class="entry__meta">
                                <span class="entry__meta-links">
                                    <a href="{{route('single.event',['id'=>$event->id])}}">Read More ..</a> 
                                   
                                </span>
                            </div>
                        </div>
                    </article> <!-- end article -->

				@endforeach
				
				

               

               

            </div> <!-- end masonry -->
		</div> <!-- end masonry-wrap -->
		
		
				<div class="row">
					<div class="col-full">
						<nav class="pgn">
							{{$events->links()}}
						</nav>
					</div>
				</div>

				
       

    </section> <!-- s-content -->
@endsection


		