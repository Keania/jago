<!DOCTYPE html>
<!-- saved from url=(0051)https://html5up.net/uploads/demos/future-imperfect/ -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>{{config('name','Jago\'s Blog')}}</title>
		
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
        <link rel="stylesheet" href="{{asset('css/main.css')}}">
        <link rel="stylesheet" href="{{asset('css/style.css')}}">

		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body class="">
	<div id="header-wrapper">
					<!-- Header -->
					<header id="header">
						<h1><a href="{{route('home')}}">Jago's Blog</a></h1>
						<nav class="links">
							<ul>
								<li><a href="{{route('home')}}">Home</a></li>
								<li><a href="{{route('blog')}}">Blog</a></li>
								<li><a href="{{route('about')}}">About Me</a></li>
								<li><a href="{{route('contact')}}">Contact Me</a></li>
								
							</ul>
						</nav>
						<nav class="main">
							<ul>
								<li class="search" style="padding-top:12px;">
									<a class="fa fa-search" href="#search">Search</a>
									<form id="search" method="get" action="#">
										<input type="text" name="query" placeholder="Search" />
									</form>
								</li>
								<li class="menu" style="padding-top:12px;">
									<a class=" fa fa-bars" href="#menu">Menu</a>
								</li>
							</ul>
						</nav>
					</header>
				</div>
		<!-- Wrapper -->
			<div id="wrapper">

			

				<!-- Menu -->
					<section id="menu">

						<!-- Search -->
							<section>
								<form class="search" method="get" action="{{route('search')}}">
									<input type="text" name="search" placeholder="Search" />
								</form>
							</section>

						<!-- Links -->
							<section>
								<ul class="links">
                                <li><a href="{{route('home')}}">Home</a></li>
								<li><a href="{{route('blog')}}">Blog</a></li>
								<li><a href="{{route('about')}}">About Me</a></li>
								<li><a href="{{route('contact')}}">Contact Me</a></li>
							</section>

						

					</section>
					

				<!-- Main -->
					<div id="main">

						<!-- Post -->
					@if(count($posts) > 0)
						@foreach($posts as $post)
						
						
							<article class="post">
								<header>
									<div class="title">
										<h2><a href="{{route('blog.single',['id'=>$post->id])}}">{{$post->title}}</a></h2>
										
									</div>
									
								</header>
								<a href="{{route('blog.single',['id'=>$post->id])}}" class="image featured"><img src='{{asset("$post->image")}}' alt=""></a>
								<p>{{substr($post->body,0,200)}}.....</p>
								<footer>
									<ul class="actions">
										<li><a href="{{route('blog.single',['id'=>$post->id])}}" class="button big">Continue Reading</a></li>
									</ul>
									<ul class="stats">
										
										<li><a href="#" class="icon fa-comment">{{count($post->comments()->getResults())}}</a></li>
									</ul>
								</footer>
							</article>
						@endforeach


						<!-- Pagination -->
							<ul class="actions pagination">
								<!-- <li><a href="https://html5up.net/uploads/demos/future-imperfect/" class="disabled button big previous">Previous Page</a></li>
								<li><a href="#" class="button big next">Next Page</a></li> -->
								{{$posts->links()}}
                            </ul>
                    @else
                    <div class="xy_text_center" style="margin-top:70px;">
                        <h1>Your Search Does Not Match Any Post</h1>
                        <a class="back" href="{{route('home')}}"  style="background-color:#2ebaae ;color:white;padding:15px;border-radius:3px;"><i class="fa fa-chevron-left"></i> Back Home</a>
                    </div>
                    @endif

					</div>

				<!-- Sidebar -->
					

			</div>

		<!-- Scripts -->
		<script src="{{asset('js/jquery.min.js')}}"></script>
			<script src="{{asset('js/browser.min.js')}}"></script>
			<script src="{{asset('js/breakpoints.min.js')}}"></script>
			<script src="{{asset('js/util.js')}}"></script>
			<script src="{{asset('js/main.js')}}"></script>

	
		</body>
			</html>