<!DOCTYPE html>
<html class="no-js" lang="en">
<head>

    <!--- basic page needs
    ================================================== -->
    <meta charset="utf-8">
    <title>Jago's Blog</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- mobile specific metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="{{asset('front/css/base.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/vendor.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/main.css')}}">

    <!-- script
    ================================================== -->
    <script src="{{asset('front/js/modernizr.js')}}"></script>
    <script src="{{asset('front/js/pace.min.js')}}"></script>

    <!-- favicons
    ================================================== -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
	<style>
		.fa {
			margin-right:0.5rem;
		}
	
	</style>

</head>

<body id="top">

		<div class="">
			@if(session()->has('success'))
				<div class="alert-box alert-box--success hideit">
                    <p>{{session()->get('success')}}</p>
                    <i class="fa fa-times alert-box__close"></i>
                </div>
			@elseif(session()->has('error'))
				<div class="alert-box alert-box--error hideit">
                    <p>{{session()->get('error')}}</p>
                    <i class="fa fa-times alert-box__close"></i>
                </div>
			@endif
		</div>

    <!-- pageheader
    ================================================== -->
    <section class="s-pageheader @yield('extra-home__classes')">

        <header class="header">
            <div class="header__content row">

                <div class="header__logo">
                    <a class="logo" href="{{route('home')}}" style="color:white;font-size:2.5rem;">
						Emmanuel Jago
                    </a>
                </div> <!-- end header__logo -->

                <ul class="header__social">
                    <li>
                        <a href="#0"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    </li>
                    <li>
                        <a href="#0"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    </li>
                    <li>
                        <a href="#0"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    </li>
                    <li>
                        <a href="#0"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                    </li>
                </ul> <!-- end header__social -->

                <a class="header__search-trigger" href="#0"></a>

                <div class="header__search">

                    <form role="search" method="get" class="header__search-form" action="{{route('search')}}">
                        <label>
                            <span class="hide-content">Search for:</span>
                            <input type="search" class="search-field" placeholder="search a post" value="" name="search" title="Search for:" autocomplete="off">
                        </label>
                        <input type="submit" class="search-submit" value="Search">
                    </form>
        
                    <a href="#0" title="Close Search" class="header__overlay-close">Close</a>

                </div>  <!-- end header__search -->


                <a class="header__toggle-menu" href="#0" title="Menu"><span>Menu</span></a>

                <nav class="header__nav-wrap">

                    <h2 class="header__nav-heading h6">Site Navigation</h2>

                    <ul class="header__nav">

                        <li class="{{request()->is('/') ? 'current' : ''}}">
							<a href="{{route('home')}}" title="">Home</a>
						</li>
                        
                        <li class="{{request()->is('blog') ? 'current' : ''}}">
                            <a href="{{route('blog')}}" title="">Blog</a>
                        </li>
                        <li class="{{request()->is('event') ? 'current' : ''}}">
							<a href="{{route('event')}}" title="">Event</a>
						</li>
                        <li class="{{request()->is('about') ? 'current' : ''}}">
							<a href="{{route('about')}}" title="">About</a>
						</li>
                        <li class="{{request()->is('contact') ? 'current' : ''}}">
							<a href="{{route('contact')}}" title="">Contact</a>
						</li>
                    </ul> <!-- end header__nav -->

                    <a href="#0" title="Close Menu" class="header__overlay-close close-mobile-menu">Close</a>

                </nav> <!-- end header__nav-wrap -->

            </div> <!-- header-content -->
        </header> <!-- header -->

		@yield('page-header-content')
        <!-- end pageheader-content row -->

    </section> <!-- end s-pageheader -->


    <!-- s-content
    ================================================== -->
   @yield('page-content')


    <!-- s-extra
    ================================================== -->
   

    <!-- s-footer
    ================================================== -->
    <footer class="s-footer">

        <div class="s-footer__main">
            <div class="row">
                
                <div class="col-two md-four mob-full s-footer__sitelinks">
                        
                    <h4>Quick Links</h4>

                    <ul class="s-footer__linklist">
                        <li><a href="{{route('home')}}">Home</a></li>
                        <li><a href="{{route('blog')}}">Blog</a></li>
                        <li><a href="{{route('event')}}">Events</a></li>
                        <li><a href="{{route('about')}}">About</a></li>
                        <li><a href="{{route('contact')}}">Contact</a></li>
                       
                    </ul>

                </div> <!-- end s-footer__sitelinks -->

               

                <div class="col-two md-four mob-full s-footer__social">
                        
                    <h4>Social</h4>

                    <ul class="s-footer__linklist">
                        <li><a href="#0">Facebook</a></li>
                        <li><a href="#0">Instagram</a></li>
                        <li><a href="#0">Twitter</a></li>
                        <li><a href="#0">Pinterest</a></li>
                        <li><a href="#0">Google+</a></li>
                        <li><a href="#0">LinkedIn</a></li>
                    </ul>

                </div> <!-- end s-footer__social -->

                <div class="col-five md-full end s-footer__subscribe">
                        
                    <h4>Our Newsletter</h4>

                    <p>Get my free book</p>

                    <div class="subscribe-form">
                        <form class="group" novalidate="true" action="{{route('add.getbook')}}" method="POST">
							{{csrf_field()}}

							<div class="form-group row">
								<div class="col-md-6 col-sm-12">
									<input type="text" name="name" placeholder="Your name" required>

								</div>
								<div class="col-md-6 col-sm-12" style="display:inline-block">
									<input type="text" value="" name="email"  placeholder="Email Address" required="">
								</div>
							</div>

							<div class="form-group">
								<input type="submit" name="subscribe" value="Send">
							</div>

						
                
                        </form>
                    </div>

                </div> <!-- end s-footer__subscribe -->

            </div>
        </div> <!-- end s-footer__main -->

        <div class="s-footer__bottom">
            <div class="row">
                <div class="col-full">
                    <div class="s-footer__copyright">
                        <span>© Copyright Philosophy 2018</span> 
                        <span>Site Template by <a href="https://colorlib.com/">Colorlib</a></span>
                    </div>

                    <div class="go-top">
                        <a class="smoothscroll" title="Back to Top" href="#top"></a>
                    </div>
                </div>
            </div>
        </div> <!-- end s-footer__bottom -->

    </footer> <!-- end s-footer -->


    <!-- preloader
    ================================================== -->
    <div id="preloader">
        <div id="loader">
            <div class="line-scale">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </div>


    <!-- Java Script
    ================================================== -->
    <script src="{{asset('front/js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('front/js/plugins.js')}}"></script>
	<script src="{{asset('front/js/main.js')}}"></script>
	

</body>

</html>