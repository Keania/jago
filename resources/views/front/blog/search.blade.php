@extends('front.app')

@section('page-content')
<section class="s-content">
        
        <div class="row masonry-wrap">
            <div class="masonry">
           

                <div class="grid-sizer"></div>
               
				@if(count($posts) > 0)
					@foreach($posts as $post)

						<article class="masonry__brick entry format-standard" data-aos="fade-up">
								
							<div class="entry__thumb" style="height:120px;">
								<a href="{{route('blog.single',['id'=>$post->id])}}" class="entry__thumb-link">
									<img src="{{asset('storage/posts/'.$post->image)}}" 
											alt="">
								</a>
							</div>
			
							<div class="entry__text">
								<div class="entry__header">
									
									<div class="entry__date">
										<a href="{{route('blog.single',['id'=>$post->id])}}">{{date('F d. Y',strtotime($post->created_at))}}</a>
									</div>
									<h1 class="entry__title"><a href="{{route('blog.single',['id'=>$post->id])}}">{{$post->title}}</a></h1>
									
								</div>
								<div class="entry__excerpt">
									<p>
										{{substr($post->body,0,200)}}
									</p>
								</div>
								<div class="entry__meta">
									<span class="entry__meta-links">
										<a href="{{route('blog.single',['id'=>$post->id])}}">Read More ..</a> 
									
									</span>
								</div>
							</div>
			
						</article> <!-- end article -->

					@endforeach
				@else
					
						<div class="" style="position:relative;top:-2rem;z-index:3;height:10rem;text-align:center">
							<p> Your search did not return any result please go back to the blog <a class="btn btn--primary" href="{{route('blog')}}">Blog Home</a></p>
						</div>
					
				@endif
				
				

               

               

            </div> <!-- end masonry -->
		</div> <!-- end masonry-wrap -->

			@if(count($posts) > 0)
			
			
					<div class="row">
						<div class="col-full">
							<nav class="pgn">
								{{$posts->links()}}
							</nav>
						</div>
					</div>
			@endif

				
       

    </section> <!-- s-content -->
@endsection


		