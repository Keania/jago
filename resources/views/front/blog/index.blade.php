@extends('front.app')

@section('page-content')
<section class="s-content">
        
        <div class="row masonry-wrap">
            <div class="masonry">
           

                <div class="grid-sizer"></div>
               

                @foreach($posts as $post)

                    <article class="masonry__brick entry format-standard" data-aos="fade-up">
                            
                        <div class="entry__thumb" style="height:120px;">
                            <a href="{{route('blog.single',['id'=>$post->id])}}" class="entry__thumb-link">
                                <img src="{{asset('storage/posts/'.$post->image)}}" 
                                        alt="">
                            </a>
                        </div>
        
                        <div class="entry__text">
                            <div class="entry__header">
                                
                                <div class="entry__date">
                                    <a href="{{route('blog.single',['id'=>$post->id])}}">{{date('F d. Y',strtotime($post->created_at))}}</a>
                                </div>
                                <h1 class="entry__title"><a href="{{route('blog.single',['id'=>$post->id])}}">{{$post->title}}</a></h1>
                                
                            </div>
                            <div class="entry__excerpt">
                                <p>
                                    {{substr($post->body,0,200)}}
                                </p>
                            </div>
                            <div class="entry__meta">
                                <span class="entry__meta-links">
                                    <a href="{{route('blog.single',['id'=>$post->id])}}">Read More ..</a> 
                                   
                                </span>
                            </div>
                        </div>
        
                    </article> <!-- end article -->

				@endforeach
				
				

               

               

            </div> <!-- end masonry -->
		</div> <!-- end masonry-wrap -->
		
		<!-- <div class="row">
					<div class="col-full">
						<nav class="pgn">
							<ul>
								<li><a class="pgn__prev" href="#0">Prev</a></li>
								<li><a class="pgn__num" href="#0">1</a></li>
								<li><span class="pgn__num current">2</span></li>
								<li><a class="pgn__num" href="#0">3</a></li>
								<li><a class="pgn__num" href="#0">4</a></li>
								<li><a class="pgn__num" href="#0">5</a></li>
								<li><span class="pgn__num dots">…</span></li>
								<li><a class="pgn__num" href="#0">8</a></li>
								<li><a class="pgn__next" href="#0">Next</a></li>
							</ul>
						</nav>
					</div>
				</div> -->
				<div class="row">
					<div class="col-full">
						<nav class="pgn">
							{{$posts->links()}}
						</nav>
					</div>
				</div>

				
       

    </section> <!-- s-content -->
@endsection


		