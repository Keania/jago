@extends('front.app')
@section('page-content')
<section class="s-content s-content--narrow s-content--no-padding-bottom">

<article class="row format-gallery">

	<div class="s-content__header col-full">
		<h1 class="s-content__header-title">
			{{$post->title}}
		</h1>
		<ul class="s-content__header-meta">
			<li class="date">{{date('F d, Y',strtotime($post->created_at))}}</li>
			
		</ul>
	</div> <!-- end s-content__header -->

	<div class="s-content__media col-full">
		<div class="s-content__slider slider">
			<div class="slider__slides">
				<div class="slider__slide">
					<img src="{{asset('storage/posts/'.$post->image)}}" 
						 
						 sizes="(max-width: 2000px) 100vw, 2000px" alt="" >
				</div>
				
			</div>
		</div>
	</div> <!-- end s-content__media -->

	<div class="col-full s-content__main">

		
		
		<p>{{$post->body}}
		</p>

		
		@php
			$prevId = (int)$post->id - 1;
			$nextId = (int)$post->id + 1;

			if($prevId > 0 ){
				$prevPost = App\Post::find($prevId);
			}

			try{
				$nextPost = App\Post::findOrFail($nextId);

			}catch(ModelNotFoundException $e){
				$nextPost = null;
			}catch(Exception $e){
				$nextPost = null;
			}

		@endphp
		<div class="s-content__pagenav">
			<div class="s-content__nav">
				@if(isset($prevPost))
					<div class="s-content__prev">
						<a href="{{route('blog.single',['id'=>$prevId])}}" rel="prev">
							<span>Previous Post</span>
							{{$prevPost->title}}
						</a>
					</div>
				@endif
				@if(isset($nextPost))
					<div class="s-content__next">
						<a href="{{route('blog.single',['id'=>$nextId])}}" rel="next">
							<span>Next Post</span>
							{{$nextPost->title}}
						</a>
					</div>
				@endif
			</div>
		</div> <!-- end s-content__pagenav -->

	</div> <!-- end s-content__main -->

</article>


<!-- comments
================================================== -->
<div class="comments-wrap">

	<div id="comments" class="row">
		<div class="col-full">

			<h3 class="h2">{{count($post->comments)}} Comment(s)</h3>

			<!-- commentlist -->
			<ol class="commentlist">

			@if($post->comments)

				@foreach($post->comments as $comment)
					<li class="depth-1 comment">

						<div class="comment__content">

							<div class="comment__info">
								<cite>{{substr($comment->email,0,5)}}xxxxx</cite>

								<div class="comment__meta">
									<time class="comment__time">{{date('M d Y H:i:m a',strtotime($comment->created_at))}}</time>
									
								</div>
							</div>

							<div class="comment__text">
							<p>{{$comment->comment}}</p>
							</div>

						</div>

					</li> <!-- end comment level 1 -->
				@endforeach
			@endif

			
			

			</ol> <!-- end commentlist -->


			<!-- respond
			================================================== -->
			<div class="respond">

				<h3 class="h2">Add Comment</h3>

				<form name="contactForm" id="contactForm" method="post" action="{{route('comment.post')}}">
					{{csrf_field()}}
					<input type="hidden" name="post_id" value="{{$post->id}}">
					<fieldset>

					

						<div class="form-field">
								<input name="email" type="text" id="cEmail" class="full-width" placeholder="Your Email" value="">
						</div>

					
						<div class="message form-field">
							<textarea name="comment" id="cMessage" class="full-width" placeholder="Your Message"></textarea>
						</div>

						<button type="submit" class="submit btn--primary btn--large full-width">Submit</button>

					</fieldset>
				</form> <!-- end form -->

			</div> <!-- end respond -->

		</div> <!-- end col-full -->

	</div> <!-- end row comments -->
</div> <!-- end comments-wrap -->

</section> <!-- s-content -->
@endsection