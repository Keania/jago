@extends('front.app')
@section('page-content')
<section class="s-content s-content--narrow">

<div class="row">

    

    <div class="col-full s-content__main">

       
        <h3>Say Hello.</h3>
        <p>You can drop me a message in the box below and i will response to all your questions</p>

        <form name="cForm" id="cForm" method="POST" action="{{route('contactUs')}}" >
            {{csrf_field()}}
            <fieldset>

                <div class="form-field">
                    <input name="name" type="text" id="cName" class="full-width" placeholder="Your Name" value="">
                </div>

                <div class="form-field">
                    <input name="email" type="text" id="cEmail" class="full-width" placeholder="Your Email" value="">
                </div>

               

                <div class="message form-field">
                <textarea name="message" id="cMessage" class="full-width" placeholder="Your Message" ></textarea>
                </div>

                <button type="submit" class="submit btn btn--primary full-width">Submit</button>

            </fieldset>
        </form> <!-- end form -->


    </div> <!-- end s-content__main -->

</div> <!-- end row -->

</section> <!-- s-content -->
@endsection