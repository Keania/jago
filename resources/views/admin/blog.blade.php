<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
   
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    
    <link rel="stylesheet" href="{{asset('dashboard/https/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('dashboard/https/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('dashboard/https/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('dashboard/https/themify-icons.css')}}">
 
    <link rel="stylesheet" href="{{asset('dashboard/https/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{asset('dashboard/assets/css/cs-skin-elastic.css')}}">
    <link rel="stylesheet" href="{{asset('dashboard/assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" href="{{asset('css/dashboard.css')}}">
    <title>{{config('name','Emmanuel Jago')}}</title>
</head>
<body>
    <div class="msg-bag">
        @if(session()->has('error'))
            <div class="error">{{session()->get('error')}}</div>
        @elseif(session()->has('success'))
            <div class="success">{{session()->get('success')}}</div>
        @endif
    </div>
    <div class="all">
        <div id="main">
            <div id="sm-menu" class=" xy_display_none nav-menu">

                <ul id="sm-screen-nav-ul">
                    <li><a href="{{route('dashboard')}}">Home</a></li>
                <li><a href="{{route('admin.blog')}}">Blog Controller</a></li>
                    <li><a href="{{route('admin.event')}}">EventController</a></li>
                <li><a href="{{route('admin.book')}}">Book Controller</a></li>
                <li><a href="{{route('admin.misc')}}">Misc</a></li>
                </ul>
            </div>
            <div id="menu_button">
                <button><i class="icon fa-bars"></i> Menu</button>
            </div>
            <header id="header">
            <nav class="links">
                <ul>
                    <li><a href="{{route('dashboard')}}">Home</a></li>
                   
                    <li><a href="{{route('admin.blog')}}">Blog Controller</a></li>
                    <li><a href="{{route('admin.event')}}">Event Controller</a></li>
                   
                    <li><a href="{{route('admin.book')}}">Book Controller</a></li>
                    <li><a href="{{route('admin.misc')}}">Misc</a></li>
                </ul>
            </nav>
            </header> 
            <div class="xy_section xy_height_30"></div>
            <div id="post-container">
                <!-- <span id="post_add_toggler" onclick="document.getElementById('add_post').classList.toggle('xy_display_none')"><i class="fa fa-plus"></i></span> -->
                <div id="add_post" class="xy_bg_white xy_grey_border">
                    <h1 >Add Post</h1>
                    <form id="add_post_form" method="POST" enctype="multipart/form-data" action="{{route('add.post')}}">
                        {{csrf_field()}}
                        <div class="group">
                            <input type="text" name="title" placeholder="Post Tile">
                        </div>
                        <div class="group">
                            <input type="file" name="image">
                        </div>
                        <div class="group">
                            <textarea name="body" rows="10" placeholder="Post Body"></textarea>
                        </div>
                        <div class="group">
                            <input type="submit" name="submit">
                        </div>
                    </form>
                    @foreach($posts as $post)
                        <form class="edit_post xy_display_none" id="edit{{$post->id}}"  method="POST" enctype="multipart/form-data" action="{{route('update.post',['id'=>$post->id])}}">
                            {{csrf_field()}}
                            <div class="group">
                                <input type="text" name="title" placeholder="Post Tile" value="{{$post->title}}">
                            </div>
                            <div class="group">
                                <input type="file" name="image">
                            </div>
                            <div class="group">
                                <textarea name="body" rows="10" placeholder="Post Body" >{{$post->body}}</textarea>
                            </div>
                            <div class="group">
                                <input type="submit" name="submit">
                                <input type="reset" name="reset" onclick="addPost();">
                            </div>
                        </form>
                    @endforeach
                </div>
                <div id="main_posts">
                    <h1>My Post</h1>
                    
                    @foreach($posts as $post)
                    <div class="xy_post_pane">
                        <div id="post-image">
                            <img class="small-img-thumbnail" src='{{asset("storage/posts/$post->image")}}'>
                        </div>
                        <div id="post-subject">
                            <span class="xy_font_1 xy_text_upper xy_font_bold">{{$post->title}}</span> 
                            <div id="post_buttons">
                                <span class="xy_color_white xy_post_buttons xy_bg_cblue xy_font_1" onclick="document.getElementById('delete{{$post->id}}').submit();">Delete</span>
                                <form id="delete{{$post->id}}"class="xy_display_none" action="{{route('delete.post',['id'=>$post->id])}}" method="POST" >{{csrf_field()}}</form>
                                <span class="xy_color_white xy_post_buttons xy_bg_cblue xy_font_1 xy_m_left_10" onclick="show_edit('edit{{$post->id}}');">Edit</span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    {{$posts->links()}}
                </div>
            </div>
        </div>
    </div>
    <script>
        var showedit = false;

        function show_edit(item){
            showedit = !showedit;
          
            item = document.getElementById(item);
            if(showedit){
                document.getElementById('add_post_form').classList.add('xy_display_none');
            }
            var edit_list = document.querySelectorAll('.edit_post');
                edit_list.forEach(function(post){
                    post.classList.add('xy_display_none');
                });
                item.classList.remove('xy_display_none');
        }

        var addPost = function(){
            showedit = !showedit;
         
            document.getElementById('add_post_form').classList.remove('xy_display_none');
            var edit_list = document.querySelectorAll('.edit_post');
                edit_list.forEach(function(post){
                    post.classList.add('xy_display_none');
                });
        }

        var sm_menu = (function(){

            document.getElementById('menu_button').addEventListener('click',function(){

                var sm_menu =  document.getElementById('sm-menu');
                sm_menu.classList.toggle('xy_display_none');
                sm_menu.classList.toggle('display_anim');

                if(sm_menu.classList.contains('xy_display_none')){

                  
                    sm_menu.style.display = 'none';

                }else{

                   
                    sm_menu.style.display = 'block';
                }

              
               
            });
        })();

       

      
    </script>
</body>
</html>