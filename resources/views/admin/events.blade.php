<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
   
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" href="{{asset('css/dashboard.css')}}">
    <title>{{config('name','Emmanuel Jago')}}</title>
   
</head>
<body>
    <div class="msg-bag">
        @if(session()->has('error'))
            <div class="error">{{session()->get('error')}}</div>
        @elseif(session()->has('success'))
            <div class="success">{{session()->get('success')}}</div>
        @endif
    </div>
    <div class="all">
        <div id="main">
            <div id="sm-menu" class=" xy_display_none nav-menu">

                <ul id="sm-screen-nav-ul">
                    <li><a href="{{route('dashboard')}}">Home</a></li>
                   <li><a href="{{route('admin.blog')}}">Blog Controller</a></li>
                    <li><a href="{{route('admin.event')}}">EventController</a></li>
                   <li><a href="{{route('admin.book')}}">Book Controller</a></li>
                   <li><a href="{{route('admin.misc')}}">Misc</a></li>
                </ul>
            </div>
            <div id="menu_button">
                <button><i class="icon fa-bars"></i> Menu</button>
            </div>
            <header id="header">
                <nav class="links">
                    <ul>
                        <li><a href="{{route('dashboard')}}">Home</a></li>
                    
                        <li><a href="{{route('admin.blog')}}">Blog Controller</a></li>
                        <li><a href="{{route('admin.event')}}">Event Controller</a></li>
                    
                        <li><a href="{{route('admin.book')}}">Book Controller</a></li>
                        <li><a href="{{route('admin.misc')}}">Misc</a></li>
                    </ul>
                </nav>
            </header> 
            <div class="xy_section xy_height_30"></div>
            <div id="post-container">
                <!-- <span id="post_add_toggler" onclick="document.getElementById('add_post').classList.toggle('xy_display_none')"><i class="fa fa-plus"></i></span> -->
                <div id="add_post" class="xy_bg_white xy_grey_border">
                   
                    <form id="add_post_form" method="POST" enctype="multipart/form-data" action="{{route('add.event')}}">
                    <caption >Add an Event</caption>
                        {{csrf_field()}}
                        <div class="group">
                            <input type="text" name="title" placeholder="Post Tile">
                        </div>
                        <div class="group">
                            <input type="file" name="image">
                        </div>
                        <div class="group">
                            <textarea name="body" rows="10" placeholder="Post Body"></textarea>
                        </div>
                        <div class="group">

                            <input type="text" name="venue" placeholder="" >
                        </div>

                        <div class="group">

                            <input type="date" name="date" placeholder="date" >
                        </div>

                        <div class="group">

                            <input type="time" name="time" placeholder="time" >
                        </div>
                        <div class="group">
                            <input type="submit" name="submit">
                        </div>
                    </form>
                    @foreach($events as $event)
                        <form class="edit_post xy_display_none" id="edit{{$event->id}}"  method="POST" enctype="multipart/form-data" action="{{route('update.event',['id'=>$event->id])}}">
                        <caption>Edit an Event</caption>
                            {{csrf_field()}}
                            <div class="group">
                                <input type="text" name="title" placeholder="Post Tile" value="{{$event->title}}">
                            </div>
                            <div class="group">
                                <input type="file" name="image"  width="90" height="30">
                            </div>
                            <div class="group">
                                <textarea name="body" rows="10" placeholder="Post Body" >{{$event->body}}</textarea>
                            </div>
                            <div class="group">

                                <input type="text" name="venue" placeholder="" value="{{$event->venue}}">
                            </div>

                            <div class="group">

                                <input type="date" name="date" placeholder="date" value="{{$event->date}}">
                            </div>

                            <div class="group">

                                <input type="time" name="time" placeholder="time" value="{{$event->time}}">
                            </div>

                            <div class="group">
                                <input type="submit" name="submit">
                                <input type="reset" name="reset" onclick="addEvent();">
                              
                            </div>
                        </form>
                    @endforeach
                </div>
                <div id="main_posts">
                    <h1>My Event</h1>
                    
                    @foreach($events as $event)
                    <div class="xy_event_pane">
                        <div id="post-image">
                            <img class="small-img-thumbnail" src='{{asset("storage/events/".$event->image)}}'>
                            <span class="event_title"class="xy_font_1 xy_text_upper xy_font_bold" style="color:white">{{$event->title}}</span> 
                        </div>
                        <div id="post-subject" class="xy_bg_white post_subject" style="padding:5px;">
                            <span ><i class="icon fa-calendar"></i>{{$event->date}}</span>
                            <span class="float_right"><i class="icon fa fa-clock-o "></i>{{$event->time}}</span>
                            <div><i class="icon fa fa-location-arrow" ></i>{{$event->venue}}</div>
                            
                            <div id="post_buttons">
                                <span class=" xy_post_buttons  xy_font_1" onclick="document.getElementById('delete{{$event->id}}').submit();"><i class="icon fa-close"></i>Delete</span>
                                <form id="delete{{$event->id}}"class="xy_display_none" action="{{route('delete.event',['id'=>$event->id])}}" method="POST" >{{csrf_field()}}</form>
                                <span class=" xy_post_buttons  float_right" onclick="show_edit('edit{{$event->id}}');"><i class="icon fa-edit"></i>Edit</span>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    {{$events->links()}}
                </div>
            </div>
        </div>
    </div>
    <script>
        var showedit = false;
        function show_edit(item){
            showedit = !showedit;
          
            item = document.getElementById(item);
            if(showedit){
                document.getElementById('add_post_form').classList.add('xy_display_none');
            }
            var edit_list = document.querySelectorAll('.edit_post');
                edit_list.forEach(function(post){
                    post.classList.add('xy_display_none');
                });
                item.classList.remove('xy_display_none');
        }

        var addEvent = function(){
            showedit = !showedit;
          
            document.getElementById('add_post_form').classList.remove('xy_display_none');
            var edit_list = document.querySelectorAll('.edit_post');
                edit_list.forEach(function(post){
                    post.classList.add('xy_display_none');
                });
        }

        var sm_menu = (function(){
            document.getElementById('menu_button').addEventListener('click',function(){
                var sm_menu =  document.getElementById('sm-menu');
                sm_menu.classList.toggle('xy_display_none');
                sm_menu.classList.toggle('display_anim');

                if(sm_menu.classList.contains('xy_display_none')){

                  
                    sm_menu.style.display = 'none';

                }else{

                   
                    sm_menu.style.display = 'block';
                }

               
            });
        })();


        var resetForm = function(form){
           console.log(form);
            form.reset();
            console.log(' i got here atleast');
        }


    </script>
</body>
</html>