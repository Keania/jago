<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
   
    <link rel="stylesheet" href="{{asset('dashboard/https/bootstrap.min.css')}}">
  
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" href="{{asset('css/dashboard.css')}}">
    <title>{{config('name','Emmanuel Jago')}}</title>
    <style>
        .table td {
            font-size: 1rem;
        }
    
    </style>
   
</head>
<body>
    <div class="msg-bag">
        @if(session()->has('error'))
            <div class="error">{{session()->get('error')}}</div>
        @elseif(session()->has('success'))
            <div class="success">{{session()->get('success')}}</div>
        @endif
    </div>
    <div class="all">
        <div id="main">
            <div id="sm-menu" class=" xy_display_none nav-menu">

                <ul id="sm-screen-nav-ul">
                    <li><a href="{{route('dashboard')}}">Home</a></li>
                   <li><a href="{{route('admin.blog')}}">Blog Controller</a></li>
                    <li><a href="{{route('admin.event')}}">EventController</a></li>
                   <li><a href="{{route('admin.book')}}">Book Controller</a></li>
                   <li><a href="{{route('admin.misc')}}">Misc</a></li>
                </ul>
            </div>
            <div id="menu_button">
                <button><i class="icon fa-bars"></i> Menu</button>
            </div>
            <header id="header">
                <nav class="links">
                    <ul>
                        <li><a href="{{route('dashboard')}}">Home</a></li>
                    
                        <li><a href="{{route('admin.blog')}}">Blog Controller</a></li>
                        <li><a href="{{route('admin.event')}}">Event Controller</a></li>
                    
                        <li><a href="{{route('admin.book')}}">Book Controller</a></li>
                        <li><a href="{{route('admin.misc')}}">Misc</a></li>
                    </ul>
                </nav>
            </header>
            <section>
                <br>
                <h5>View Registered Participants for events</h5>
                <br>
                <main>
                    <form method="POST">
                        <div class="form-group">
                            <select name="event" id="selected-event" class="form-control col-md-6 col-sm-12">
                                @foreach(App\Events::all() as $event)
                                    @if(count($event->event_interest) > 0)
                                        <option value="{{$event->id}}">{{$event->title}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <input type="submit" name="submit" class="btn btn-primary" value="View Participants" onclick="event.preventDefault();fetchEventParticipants();">
                    
                        </div>
                    

                    
                    </form>
                    <div style="width:100%;height:2rem;"></div>
                    <article id="screen">
                        <table class="table table-stripped">
                            <thead>
                                <tr>
                                    <td>
                                        Full Name
                                    </td>
                                    <td>
                                        Email
                                    </td>
                                    <td>
                                        Phone
                                    </td>
                                </tr>
                            </thead>
                            <tbody id="table-body">

                            </tbody>
                        </table>
                    </article>

                    <a href="{{route('download.emails')}}"  download>Download Emails</a>

                    
                </main>
            </section>
        </div>
    </div>

        <script>
            
            var fetchEventParticipants =  function(){

                $id  = document.getElementById('selected-event').value;
                console.log($id);
                var $url = `/event/participant/${$id}`;
                console.log($url);
                fetch($url
                ).then(function(response){
                    return response.json();
                }).then(function(myjosn){
                    var tableBody = document.getElementById('table-body');
                    tableBody.innerHTML = '';

                   myjosn.forEach(function(item){
                       var tableRow = document.createElement('tr');

                       var tableDataFull_Name = document.createElement('td');
                       tableDataFull_Name.innerHTML = item.full_name;
                       tableRow.append(tableDataFull_Name);

                       var tableDataEmail  = document.createElement('td');
                       tableDataEmail.innerHTML = item.email;
                       tableRow.append(tableDataEmail);

                       var tableDataPhone = document.createElement('td');
                       tableDataPhone.innerHTML = item.phone;
                       tableRow.append(tableDataPhone);

                       tableBody.append(tableRow);

                     
                        
                   });
                });
            }

            var sm_menu = (function(){
            document.getElementById('menu_button').addEventListener('click',function(){
                var sm_menu =  document.getElementById('sm-menu');
                sm_menu.classList.toggle('xy_display_none');
                sm_menu.classList.toggle('display_anim');

                if(sm_menu.classList.contains('xy_display_none')){

                  
                    sm_menu.style.display = 'none';

                }else{

                   
                    sm_menu.style.display = 'block';
                }

               
            });
        })();


            var downloadEmails = function(){

                $url = '/download/emails';
                fetch($url).then(function(response){

                    console.log(response.statusText);
                    alert(response.statusText);

                   

                })
            }
        
        </script>
</body>
</html>