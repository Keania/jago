<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" href="{{asset('css/dashboard.css')}}">
    <title>{{config('name','Emmanuel Jago')}}</title>
</head>
<body>
    <div class="msg-bag">
        @if(session()->has('error'))
            <div class="error">{{session()->get('error')}}</div>
        @elseif(session()->has('success'))
            <div class="success">{{session()->get('success')}}</div>
        @endif
    </div>
    <div class="all">
        <div id="main">
            <div id="sm-menu" class=" xy_display_none nav-menu">

                <ul id="sm-screen-nav-ul">
                    <li><a href="{{route('dashboard')}}">Home</a></li>
                <li><a href="{{route('admin.blog')}}">Blog Controller</a></li>
                    <li><a href="{{route('admin.event')}}">EventController</a></li>
                <li><a href="{{route('admin.book')}}">Book Controller</a></li>
                <li><a href="{{route('admin.misc')}}">Misc</a></li>
                </ul>
            </div>
            <div id="menu_button">
                <button><i class="icon fa-bars"></i> Menu</button>
            </div>
            <header id="header">
                <nav class="links">
                    <ul>
                        <li><a href="{{route('dashboard')}}">Home</a></li>
                    
                        <li><a href="{{route('admin.blog')}}">Blog Controller</a></li>
                        <li><a href="{{route('admin.event')}}">Event Controller</a></li>
                    
                        <li><a href="{{route('admin.book')}}">Book Controller</a></li>
                        <li><a href="{{route('admin.misc')}}">Misc</a></li>
                    </ul>
                </nav>
            </header> 
            <div class="xy_section xy_height_30"></div>
            <div id="book-top">
                <div id="add-book" class="xy_bg_white xy_grey_border" style="padding:15px;">
                    <h1>Add Book</h1>
                    <form method="POST" action="{{route('add.book')}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="group">
                            <input type="text" name="title" placeholder="Book Title" required>
                        </div>
                        <div class="group">
                            <input type="file" name="image">
                            <help><i>upload book image here</i></help>
                        </div>
                        <div class="group">
                            <input type="file" name="book_file" required>
                            <help><i>upload main book required</i></help>
                        </div>
                        <div class="group">
                            <input type="submit" name="submit">
                        </div>
                    </form>
                </div>
                <div id="current-book" class="xy_bg_white xy_grey_border" style="padding:15px;">
                    <h1>Current Book</h1>
                    @if($activebook)
                    
                        <div class="xy_book_pane">
                        <div id="book-image">
                            <img class="book-thumbnail" src='{{asset("storage/books/$activebook->image")}}'>
                        </div>
                        <div id="book-subject">
                            <span class="xy_font_1 xy_text_upper xy_font_bold"><a href='{{asset("storage/books/$activebook->url")}}'>{{$activebook->title}}</a></span> 
                        </div>
                    @endif
                </div>
            </div>
            
        </div>
        <div class="xy_section xy_height_30"></div>
        <h1 style="text-align:center;">Book Store</h1>
        <div id="book-container">
            
           
            @foreach($books as $book)
            
            @php
            $image = $book->image;
           
            @endphp
            <div class="xy_book_pane">
                <div id="book-image">
                    <img class="book-thumbnail" src='{{asset("storage/books/$book->image")}}'>
                </div>
                <div id="book-subject">
                    <span class="xy_font_1 xy_text_upper xy_font_bold"><a  target="_blank" href='{{asset("storage/books/$book->url")}}'>{{$book->title}}</a></span> 
                    <div id="post_buttons">
                        <span class="xy_color_white xy_post_buttons xy_bg_cblue xy_font_1" onclick="document.getElementById('activate{{$book->id}}').submit();">Activate</span>
                        <span class="xy_color_white xy_post_buttons xy_bg_cblue xy_font_1 xy_m_left_10" onclick="document.getElementById('delete{{$book->id}}').submit();">Delete</span>
                        <form class="xy_display_none" id="delete{{$book->id}}" action="{{route('delete.book',['id'=>$book->id])}}" method="POST">{{csrf_field()}}</form>
                        <form class="xy_display_none" id="activate{{$book->id}}" action="{{route('activate.book',['id'=>$book->id])}}" method="POST">{{csrf_field()}}</form>
                    </div>
               </div>
            </div>
            @endforeach
        </div>
    </div>

    <script>
              var sm_menu = (function(){
             document.getElementById('menu_button').addEventListener('click',function(){
                var sm_menu =  document.getElementById('sm-menu');
                sm_menu.classList.toggle('xy_display_none');
                sm_menu.classList.toggle('display_anim');

                if(sm_menu.classList.contains('xy_display_none')){

                  
                    sm_menu.style.display = 'none';

                }else{

                   
                    sm_menu.style.display = 'block';
                }

               
            });
        })();
    
    </script>
</body>
</html>