<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
   
    <link rel="stylesheet" href="{{asset('dashboard/https/bootstrap.min.css')}}">
 
    <title>{{config('name','Emmanuel Jago')}}</title>
    <style>
            body {
                background-color: #EBEBEB;
            }
            #header {
                padding-top:0.6rem;
                text-align: center;
            }

            .container {
                background-color: white;
                width:80%;
                margin:auto;
                margin-top: 3rem;
            }

            #form-holder{
                padding:1rem;
            }
            #divider{
                width:100%;
                height:1rem;
                background-color: #EBEBEB;
            }
            .help {
                color:crimson;
            }
    
    </style>
</head>
<body>
   
    <div class="container">
        <header id="header">
            <h5>Login</h5>
        </header>
        <div id="divider"></div>
        <div class="offset-md-2" id="form-holder">
           
            
            <form method="POST" action="{{route('login')}}" class="col-md-6">
                {{csrf_field()}}
               <div class="form-group ">
                   <label for="email" class="form-control-label">Email</label>
                   <input type="email" name="email" id="email" class="form-control">
                    @if($errors->has('email'))
                        <span class="help">{{$errors->first('email')}}</span>
                    @endif
               </div>

               <div class="form-group">
                   <label for="password" class="form-control-label">Password</label>
                   <input type="password" name="password" id="password" class="form-control">
                   @if($errors->has('password'))
                        <span class="help">{{$errors->first('password')}}</span>
                    @endif
        
               </div>

               <div class="form-group">
                   <input type="submit" name="submit"  class="btn btn-primary" value="Login">
               </div>

            </form>
        </div>
    </div>
</body>
</html>
