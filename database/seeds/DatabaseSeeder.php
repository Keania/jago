<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PostTableSeeder::class);
        $this->call(CommentTableSeeder::class);
        $this->call(BookTableSeeder::class);
        $this->call(EventTableSeeder::class);
    }
}
