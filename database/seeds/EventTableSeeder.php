<?php

use Illuminate\Database\Seeder;

class EventTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();
        $numbers = [1,2,3];

        foreach(range(1,30) as $index=>$value){
            DB::table('events')->insert([
                'title'=>$faker->sentence,
                'body'=>$faker->text($minNbChars=800,$maxNbChars=1000),
                'image'=>$faker->randomElement($numbers).'.jpg',
                'venue'=>$faker->address,
                'date'=>$faker->date(),
                'time'=>$faker->time(),
                'created_at'=>$faker->dateTime($max='now'),
                'updated_at'=>$faker->dateTime($max='now'),
            ]);
        }
    }
}
