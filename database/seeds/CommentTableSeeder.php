<?php

use Illuminate\Database\Seeder;
use App\Post;

class CommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();
        $posts = Post::all()->pluck('id')->toArray();

        foreach(range(1,60) as $index=>$value){
            DB::table('comments')->insert([
                'comment'=>$faker->sentence,
                'email'=>$faker->email,
                'post_id'=>$faker->randomElement($posts),
                'created_at'=>$faker->dateTime($max='now'),
                'updated_at'=>$faker->dateTime($max='now'),
            ]);
        }
    }
}
