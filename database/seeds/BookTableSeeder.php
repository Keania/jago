<?php

use Illuminate\Database\Seeder;

class BookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();
        $numbers = [8,9,10,11,12];

        foreach(range(1,5) as $index=>$value){
            DB::table('books')->insert([
                'title'=>$faker->sentence,
                'active'=>false,
                'url'=>$faker->randomElement($numbers).'.jpg',
                'image'=>'',
                'created_at'=>$faker->dateTime($max='now'),
                'updated_at'=>$faker->dateTime($max='now'),
            ]);
        }
        }
}
