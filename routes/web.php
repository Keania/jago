<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::middleware('auth')->group(function(){
    Route::get('/admin/dashboard','AdminController@dashboard')->name('dashboard');
    Route::get('/admin/blog','AdminController@blog')->name('admin.blog');
    Route::get('/admin/book','AdminController@book')->name('admin.book');
    Route::get('/admin/event','AdminController@event')->name('admin.event');
    Route::get('/admin/misc','AdminController@misc')->name('admin.misc');
    Route::get('/download/emails','AdminController@downloadEmails')->name('download.emails');
    
    Route::get('event/participant/{id}','AdminController@eventParticipant')->name('event.participants');

    Route::prefix('book')->group(function(){
        Route::post('add','BookController@add')->name('add.book');
        Route::post('delete/{id}','BookController@delete')->name('delete.book');
        Route::post('activate/{id}','BookController@activate')->name('activate.book');
    });
    Route::prefix('blog')->group(function(){
        Route::post('add','BlogController@add')->name('add.post');
        Route::post('update','BlogController@update')->name('update.post');
        
        Route::post('delete/{id}','BlogController@delete')->name('delete.post');
    });
    Route::prefix('events')->group(function(){
        Route::post('add','EventController@add')->name('add.event');
        Route::post('update','EventController@update')->name('update.event');
        Route::post('delete/{id}','EventController@delete')->name('delete.event');
    });


});
Route::post('ebook','EmailController@getbook')->name('add.getbook');


Auth::routes();



Route::get('/', 'HomeController@home')->name('home');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::post('/contact','HomeController@sendContactMsg')->name('contactUs');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/post/search', 'HomeController@search')->name('search');
Route::get('/blog', 'HomeController@blog')->name('blog');
Route::get('/blog/post/{id}', 'HomeController@single')->name('blog.single');
Route::post('comment','BlogController@comment')->name('comment.post');
Route::get('/event','HomeController@event')->name('event');
Route::get('/event/{id}','HomeController@single_event')->name('single.event');
Route::post('/event/interest','EventController@interestInEvent')->name('interest.event');

